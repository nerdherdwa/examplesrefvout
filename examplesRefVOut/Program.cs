﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace examplesRefVOut
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 1;
            Console.WriteLine("Ref Keyword: ");
            Console.WriteLine("Previous value of integer i : " + i.ToString());
            string test = GetNextNameRef(ref i);
            Console.WriteLine("Current value of integer i : " + i.ToString());
            Console.WriteLine();

            int j = 0;
            Console.WriteLine("Ref Keyword: ");
            Console.WriteLine("Previous value of integer j : " + j.ToString());
            string test2 = GetNextNameByOut(out j);
            Console.WriteLine("Current value of integer j : " + j.ToString());

            Console.WriteLine();
            Console.Write("Press any key to exit.");
            Console.ReadKey();
        }



        public static string GetNextNameRef(ref int id)
        {
            string returnText = "Next - " + id.ToString();
            id += 1;
            return returnText;
        }

        public static string GetNextNameByOut(out int id)
        {
            id = 1;
            string returnText = "Next - " + id.ToString();
            return returnText;
        }
    }
}
